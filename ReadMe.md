# MASTI - Messenger Application for Student-Teacher Interaction.

An application that facilitates interaction between multiple students and a teacher.     
_Project as part of **Principles of Software Engineering** course, @ IIT Palakkad._            

* Live screen-sharing support included.
* Facilitates texting between student and teacher. 

# Repository Structure

The folders corresponding to each module contains _(should contain)_:

- A folder `Specs` contains the specs of the team leader and team members.
- The team leader's spec is titled `ReadMe.md`.
- Team members' specs are titled `ROLLNUMBERFirstname.md`.
- A folder for each Markdown file, which contains the images used in the file. 
- Other files and folders concerning the code.  


# Authorization:

- Team leads will fork this repository and send pull requests. 
- Pull requests to be entertained _only_ if they're from the team leads. 
- Architect to be added as a collaborator. 

# Evaluation methodology

- Everything will be evaluated on the BitBucket repository, no new submission portals will be created. 
- Ram sir will be given write access to the repository, so that he could evaluate and suggest edits. 

# Standardisation norms

## Documentation

- All design documents _must_ be in markdown format. 
- All design specs _must_ be in the **Specs** folder. 
- Design specs include the specs of Team Leads and individual team members. 

### Team Lead's document:

- Components that you will depend on, and the components that in turn will depend on. 
- The interfaces you are going to expose/consume (some pseudo code). 
- The various internal components involved, the developers for each of the component and also any test code developers. 
- High level class diagram, activity diagram. 

### Individual member's document:

- Details for each internal component you are working on. 
- The classes involved etc. 

## UML Diagrams
### Class Diagram
- Team lead must submit a UML diagram of all the internal class dependencies within the concerned module. _(This is needed for the Architect's master UML diagram.)_ 
### Module Diagram
- A module diagram shows dependencies of your module on rest of the modules. 
### Activity Diagram
- Activity diagram is like a flow-chart which would indicate how the control flows through your module. 

## Naming Files and Folders

- A folder `Specs` contains the specs of the team leader and team members.
- The team leader's spec is titled `ReadMe.md` and is located in `Specs` folder. 
- All the images used in `ReadMe.md` to go in folder `Specs/ReadMe`. 
- Team members' specs would be titled `111501001Firstname.md` and to be located in `Specs` folder.
- All the images used in `111501001Firstname.md` to go in folder `Specs/111501001Firstname`. 
- Other files and folders concerning the code.  

## Coding Standards
* Enforced using StyleCop and FxCop. 
* Files could be found in Templates folder

### Header format:
```Csharp
//-----------------------------------------------------------------------
// <author> 
//     Author's full name 
// </author>
//
// <date> 
//     dd-mm-yyyy 
// </date>
// 
// <reviewer> 
//     Reviewer's name 
// </reviewer>
// 
// <copyright file="ICommunication.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//		A brief description of the file.
// </summary>
//-----------------------------------------------------------------------
```

## About Course

### COURSE SCOPE
This course is concerned with how to design and develop a reasonably big software – big enough that it warrants multiple phases (and multiple developers potentially) – such as for example a distributed system - including architecture, design, and implementation. Good software development mandates that we do a set of tasks before committing to code.  
  
**Learning Objectives:**
1.  Software development lifecycle
2.	Software design.
3.	Writing design specs.
4.	Coding standards and guidelines.
5.	Source depot and version control systems.
6.	Debugging.
7.	Working in groups.
8.	Working on shared code.
9.	Code reviewing.
10.	Integrating and testing your code.
11.	Demo-ing your project to the customer (your class and your faculty in this case).
12.	Plus familiarity with the specific project that you are assigned.

### RESOURCES

**BOOKS:**
1.	Design Patterns: Elements of Reusable Object-Oriented Software by Ralph Johnson · Erich Gamma · Richard Helm · John Vlissides
2.	Software Project Survival Guide by Steve McConnell
3.	The Mythical Man-Month by Fred Brooks
4.	Joel on Software by Joel Spolsky  
  
**WEB RESOURCES**:
1.	UML: UML (embarcadero.com), UML (uml-diagrams.org)
2.	XML: https://www.w3schools.com/ 
3.	C#: video classes here, programming guide here  
  
### PREREQUISITES  
Students are expected to know basic programming – strings, arrays and pointer basics, dynamic memory allocation, data structures.

### GOAL
Client-Server Messaging
1.	Server side shall allow multiple clients to connect to it, and shall support messaging to any of the clients independently.
2.	Client side shall allow connecting to the server, and send/receive messages.
3.	An extended goal is to let a client share their screen with the server. You can think of it at a barebones level: how you could capture the screen image and send it across to the server? At what frequency? The server should be allowed to request the images on demand etc.

### PARTICIPANT ROLES
**Feature teams:**
Each feature team will have one leader. All the team members including the leader will write a design spec – detailing what part of the team component they are going to design, and how it fits/interfaces with the other components, the design options available, your design choice, and analysis. One of the members can take care of the test suite for your component.
The feature teams and the number of members are:  
*	The user interface team – 5 | `https://bitbucket.org/varshith-polu/pse-project`  
*	The image processing team - 5 | `https://bitbucket.org/soorajtom/pse-project`  
*	The messaging team - 5 | `https://bitbucket.org/gautam16kr/pse-project-messaging`  
*	The persistence team  - 3 | `https://bitbucket.org/prebel/pse-project`  
*	The schema team - 3 | `https://bitbucket.org/akshat3011/pse-project`  
*	The networking team - 5 | `https://bitbucket.org/PArthPAtel007/pse-project`    
  
**Quality team:**
This team shall help validate the project over its various development phases. You need to design a test harness to which each team will write their tests against. You’ll need to spec as well as code the test harness – along with any other tools that the team agrees on.
The team and the number of members are:
*	The tools and validation team – 3 | `https://bitbucket.org/anish-mm/pse-project`  
  
**Leadership team:**
The leadership team shall make sure that the project is delivered successfully. Scheduling, checkpoints, making sure the teams are communicating, resolving any potential conflicts etc. You’ll also need to write a spec encompassing the overall design of the feature – not just of any individual component as such, the various design patterns, analysis of the design, future improvements etc. As for coding, chip in as required although it is not mandatory.
The members are:
*	Project manager – 1 (scheduling, checkpoints, communication)
*	Architect – 1 (overall design, interfaces between various components)


### HELP
-	Course instructor: Prof. Ram (email.com)
-   Project Architect: Adithya Kumar (email.com)
-   Project Manager: Ashutosh (email.com)

--------------------------------------------------
_Readme.md by Ashutosh (111501029) + Adithya (111501017)_
