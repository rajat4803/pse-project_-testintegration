##        		INDIAN INSTITUE OF TECHNOLOGY, Palakkad

###                        			 			Principles of Software Engineering

###                                                   					        						UI TEAM	

​																			M Aditya
​																			111501016



**Objective**:

- Client side messaging and text processing UI element design.
- Code the functionalities based on events using message processing API.



**Design:**

- **Connect Screen:**

  In the client side screen the client first need  to enter  the IP address and port number of the server to connect to the server. In the client side screen the left panel takes the required information to connect to the server. 

![before_Connect](./111501016Aditya/before_Connect.png)



- **Client Side Chat screen:**

  After connecting with the server the messages sent or received are shown in the message screen space. To disconnect from the server a disconnect button is available on  the left panel.  The Image below shows a sample UI design of the chat screen and other panels.

![after_Connect](./111501016Aditya/after_Connect.png)

- **Transporting Messages:**

  When the messages are received create proper labels and populate them on the message screen space and Aligning the other messages accordingly. when the client enters a message, data is taken and sent to appropriate module.


**Buttons Description:**

1. *Connect*: Given the IP address and port number of the server. The connect button connects the client to server.
2. *Send*: Given the message the send button sends the message to the server.
3. *Screen Sharing:* This Button is used by the client to share its screen with the server. 
4. *Disconnect:* This allows the client to disconnect from the server.

**Class Diagram:**

The image below shows all the classes involved and interactions between them :



![ClientClassDiagram](./111501016Aditya/ClientClassDiagram.png)

