﻿// -----------------------------------------------------------------------
// <author> 
//      Libin N George
// </author>
//
// <date> 
//      12-10-2018 
// </date>
// 
// <reviewer>
//      Ayush Mittal
// </reviewer>
//
// <copyright file="DataRecevialNotifier.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This file contains Data Receiver Publisher 
//      Publisher - DataReceiveNotifier
//      Function for Subscribing -  SubscribeForDataReceival
//      This file is a part of Networking Module
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Net;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// Notifier for Receiving Data Component of Communication Class
    /// Subscription for receiving messages 
    /// and Triggering the Delegate on receiving message or data 
    /// </summary>
    public partial class Communication : ICommunication
    {
        /// <summary>
        /// Holds Instance on ISchema for usage in Communication
        /// </summary>
        private ISchema schema;

        /// <summary>
        /// Dictionary which maps Datatype with event handlers (which is triggered on data receival)
        /// </summary>
        private Dictionary<DataType, DataReceivalHandler> dataReceivalEventMap = new Dictionary<DataType, DataReceivalHandler>
        {
            { DataType.ImageSharing, null },
            { DataType.Message, null }
        };

        /// <summary>
        /// Gets or sets instance on ISchema for usage in Communication
        /// </summary>
        public ISchema Schema
        {
            get
            {
                return this.schema;
            }

            set
            {
                this.schema = value;
            }
        }

        /// <summary>
        /// Subscribes the Communication module for receiving messages.
        /// </summary>
        /// <param name="type">type (Message or ImageSharing) of data which should trigger corresponding event handler </param>
        /// <param name="receivalHandler">Event handler which has to be called when data with given type is received</param>
        /// <returns>true on success</returns>
        public bool SubscribeForDataReceival(DataType type, DataReceivalHandler receivalHandler)
        {
            // Protecting Dictionary from concurrent calls
            lock (this.dataReceivalEventMap)
            {
                try
                {
                    this.dataReceivalEventMap[type] += receivalHandler;
                    MastiDiagnostics.LogSuccess(string.Format(CultureInfo.CurrentCulture, "Registered a subsriber for Receiving Packet of type {0} from Network.", type.ToString()));
                    return true;
                }
                catch (KeyNotFoundException)
                {
                    MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "KeyNotFoundException on key {0}. Event Map is not initalized with given key.", type.ToString()));
                    return false;
                }
            }
        }

        /// <summary>
        /// internal method for triggering Event handlers
        /// </summary>
        /// <param name="data">Data which is received</param>
        /// <param name="fromIP">IP Address from which data is received</param>
        /// <returns>returns true on success , false on Invalid Tag </returns>
        public bool DataReceiveNotifier(string data, IPAddress fromIP)
        {
            if (fromIP == null)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "ArgumentNullException : Parmeter fromIP is Null"));
                return false;
            }

            // Finding type of Data received
            Tuple<bool, DataType> from = this.GetTagFromData(data);

            bool validFromModule = from.Item1;
            DataType fromModule;

            // Checking fromModule is valid or not
            if (validFromModule)
            {
                fromModule = from.Item2;
                MastiDiagnostics.LogInfo(string.Format(CultureInfo.CurrentCulture, "Packet Recevied from Network is of Module {0}.", fromModule.ToString()));
            }
            else
            {
                MastiDiagnostics.LogWarning(string.Format(CultureInfo.CurrentCulture, "Packet Recevied from Network is not recognized as a valid Module. Packet Dropped... "));
                return false;
            }

            // Calling Delegates
            if (this.dataReceivalEventMap[fromModule] != null)
            {
                MastiDiagnostics.LogInfo(string.Format(CultureInfo.CurrentCulture, "Calling Registered Delegate for Packet type {0}. ", fromModule.ToString()));
                this.dataReceivalEventMap[fromModule](data, fromIP);
            }
            else
            {
                MastiDiagnostics.LogWarning(string.Format(CultureInfo.CurrentCulture, "No Deligate is registered for Packet type {0}. Packet Dropped...", fromModule.ToString()));
            }

            return true;
        }
    }
}
