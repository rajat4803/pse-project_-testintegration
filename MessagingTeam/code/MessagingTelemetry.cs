﻿//-----------------------------------------------------------------------
// <author> 
//    Aryan Raj, Rahul Dhawan, Sai nishant, Vishwajeet
// </author>
//
// <date> 
//     11-oct-2018 
// </date>
// 
// <reviewer> 
//     Aryan Raj
// </reviewer>
// 
// <copyright file="MessagingTelemetry.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      The following file contain our SchemaStub.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.Messenger
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.QualityAssurance;

    /// <summary>
    /// MessagingTelemetry implementing ITelemetry
    /// </summary>
    public class MessagingTelemetry : ITelemetry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessagingTelemetry" /> class.
        /// </summary>
        public MessagingTelemetry()
        {
            this.DataCapture.Add("sendMessage", "0");
            this.DataCapture.Add("connection", "0");
            this.DataCapture.Add("dataReceived", "0");
        }

        /// <summary>
        /// Gets or sets.dictionary to store counts of no of calls
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "*")]
        public IDictionary<string, string> DataCapture { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// calculating the count of send messages
        /// </summary>
        public void CalculateSend()
        {
            int total = int.Parse(this.DataCapture["sendMessage"], CultureInfo.CurrentCulture);
            total = total + 1;
            this.DataCapture["sendMessage"] = total.ToString(CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Calculating the count of connection
        /// </summary>
        public void CalculateConnection()
        { 
            int total = int.Parse(this.DataCapture["connection"], CultureInfo.CurrentCulture);
            total = total + 1;
            this.DataCapture["connection"] = total.ToString(CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// calculates count of recived messages
        /// </summary>
        public void CalculateRec()
        {
            int total = int.Parse(this.DataCapture["dataReceived"], CultureInfo.CurrentCulture);
            total = total + 1;
            this.DataCapture["dataReceived"] = total.ToString(CultureInfo.CurrentCulture);
        }
    }
}
