//-----------------------------------------------------------------------
// <author> 
//     Aryan Raj 
// </author>
//
// <date> 
//     12-10-2018 
// </date>
// 
// <reviewer> 
//     Aryan Raj 
// </reviewer>
// 
// <copyright file="Handler.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This file implements delegates .
// </summary>
//-----------------------------------------------------------------------

namespace Masti.Messenger
{
    using System;

    /// <summary>
    /// Handler class
    /// </summary>
    public static class Handler
    {
        /// <summary>
        /// This is data receiver handler to which data receiving callbacks from UX will be subscribed 
        /// </summary>
        /// <param name="message">text message</param>
        /// <param name="toIP">IP of where messages is to be sent</param>
        /// <param name="fromIP">IP of sender</param>
        /// <param name="dateTime">date and time of message</param>
        public delegate void DataReceiverHandler(string message, string toIP, string fromIP, string dateTime);

        /// <summary>
        /// This is status receiver handler to which status receiving callbacks from UX will be subscribed
        /// </summary>
        /// <param name="status">1 or 0 for success or failure</param>
        /// <param name="fromIP">source IPe</param>
        /// <param name="message">text message</param>
        public delegate void DataStatusHandlers(StatusCode status, string fromIP, string message);

        /// <summary>
        /// This is handler to run callbacks for initial connections given by UX
        /// </summary>
        /// <param name="fromIP">source IP</param>
        /// <param name="userName">user name</param>
        public delegate void ConnectHandlers(string fromIP, string userName);

        /// <summary>
        /// enum to keep track of status i.e success or failure of sent message
        /// </summary>
        public enum StatusCode
        {
            /// <summary>
            /// Send Success 
            /// </summary>
            Success,

            /// <summary>
            /// Send Failure
            /// </summary>
            Failure,

            /// <summary>
            /// connection establish
            /// </summary>
            ConnectionEstablished,

            /// <summary>
            /// connection error
            /// </summary>
            ConnectionError
        }
    }
}
