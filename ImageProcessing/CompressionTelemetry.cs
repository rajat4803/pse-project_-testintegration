﻿//-----------------------------------------------------------------------
// <author> 
//    Amish Ranjan (amishranjanranjan@gmail.com)
// </author>
//
// <date> 
//     15th November, 2018
// </date>
// 
// <reviewer> 
//
// </reviewer>
// 
// <copyright file="CompressionTelemetry.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This is the telemetry class for the Compression class.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.ImageProcessing
{
    using System.Collections.Generic;
    using Masti.QualityAssurance;

    /// <summary>
    /// Class to handle telemetry for Compression module.
    /// </summary>
    public class CompressionTelemetry : ITelemetry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompressionTelemetry"/> class.
        /// </summary>
        public CompressionTelemetry()
        {
            this.DataCapture.Add("performanceFactor", "0");
        }

        /// <summary>
        /// Gets or sets DataCapture.
        /// This is the essential part. Should contain the data.
        /// </summary>
        public IDictionary<string, string> DataCapture { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Updates the dictionary for performance factor value
        /// </summary>
        /// <param name="performanceFactor">Gives performance factor, how well compression worked.</param>
        public void UpdatePerformance(string performanceFactor)
        {
            this.DataCapture["performanceFactor"] = performanceFactor;
        }
    }
}
