﻿//-----------------------------------------------------------------------
// <author> 
//     Suman Saurav Panda
// </author>
//
// <date> 
//     18-Nov-2018 
// </date>
// 
// <reviewer> 
//     Amish Ranjan
// </reviewer>
// 
// <copyright file="ReceiveImageTest.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This is a class intended to provide Test Cases for class ReceiveImage
//      and uses the ITest Interface. There are some saved images in specs directory
//      which are being used for testing the module.
//      For log specifications refer to QualityAssurance.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.ImageProcessing
{
    using System;
    using System.Net;
    using System.Threading;
    using Masti.QualityAssurance;
    using Masti.Schema;

    /// <summary>
    /// This class provides test cases for ReceiveImage class.
    /// </summary>
    public class ReceiveImageTest : ITest
    {
        /// <summary>
        /// this field is required to test the timer funcitonality in ReceiveImage
        /// </summary>
        private ManualResetEvent manualReset = new ManualResetEvent(false);

        /// <summary>
        /// Initializes a new instance of the <see cref="ReceiveImageTest" /> class.
        /// </summary>
        /// <param name="logger">logger object</param>
        public ReceiveImageTest(ILogger logger)
        {
            this.Logger = logger; 
            this.Schema = new StubSchema();
            this.Compression = new StubCompression();
            this.ImageCommunication = new StubImageCommunication();
            this.ReceiveImage = new ReceiveImageTestHelper(this.Schema, this.Compression, this.ImageCommunication);
            this.ReceiveImage.Start = true;
            this.ReceiveImage.RegisterListenerErrorImageDecoded(this.OnReceivingErrorUI);
            this.ReceiveImage.RegisterListenerImageDecoded(this.OnReceivingImageUI);
            this.UIErrorFlag = false;
            this.UIImageFlag = false;
            this.StopSignal = false;
            this.IP = IPAddress.Parse("10.64.231.10");
        }

        /// <summary>
        /// Gets or sets Logger instance.
        /// </summary>
        public ILogger Logger { get; set; }

        /// <summary>
        /// Gets or sets the ReceiveImage object for testing
        /// </summary>
        public ReceiveImageTestHelper ReceiveImage { get; set; }

        /// <summary>
        /// Gets or sets the Stub of ImageCommunication object for testing
        /// </summary>
        public StubImageCommunication ImageCommunication { get; set; }

        /// <summary>
        /// Gets or sets the Stub Schema of ISchema interface for testing purpose
        /// </summary>
        public ISchema Schema { get; set; }

        /// <summary>
        /// Gets or sets a stub Compression object for testing
        /// </summary>
        public ICompression Compression { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the test UI handler received error or not
        /// </summary>
        public bool UIErrorFlag { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether test UI handler received a correct image or not
        /// </summary>
        public bool UIImageFlag { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether stopsignal is set or not on error message by UI in test
        /// </summary>
        public bool StopSignal { get; set; }

        /// <summary>
        /// Gets or sets the dummy Ip address in the constructor 
        /// </summary>
        public IPAddress IP { get; set; }

        /// <summary>
        /// Run function for implementing Itest
        /// </summary>
        /// <returns>true upon passing all the test case otherwise returns false</returns>
        public bool Run()
        {
            if (this.ReceiveImageFunctionTester() && this.ReceiveImageFunctionalityTester())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// dummy UI driver handler for correct image receiving
        /// </summary>
        /// <param name="sender">default way for calling subscribed event</param>
        /// <param name="e">extra arguement needed by event handler</param>
        private void OnReceivingImageUI(object sender, ImageEventArgs e)
        {
            this.UIImageFlag = true;
        }

        /// <summary>
        /// dummy UI driver handler for error receiving
        /// </summary>
        /// <param name="sender">default way for calling subscribed event</param>
        /// <param name="e">extra arguement needed by event handler</param>
        private void OnReceivingErrorUI(object sender, ErrorEventArgs e)
        {
            if (e.StopSignalOnError)
            {
                this.StopSignal = true;
                this.manualReset.Set();
            }

            this.UIErrorFlag = true;
        }

        /// <summary>
        /// dummy function to test all the methods implemented in receiveimage
        /// </summary>
        /// <returns>true upon all passing otherwise false</returns>
        private bool ReceiveImageFunctionTester()
        {
            if (this.TestOnReceiveImage()
                && this.TestGetBitmap()
                && this.CheckOnErrorDecoded()
                && this.CheckOnImageDecoded()
                && this.TestCreateTimerObj()
                && this.TestDestroyTimerObj()
                && this.TestResetTimer())
            {
                this.Logger.LogSuccess("all method passed successfully");
                return true;
            }
            else
            {
                this.Logger.LogError("did not pass all the methods");
                return false;
            }
        }

        /// <summary>
        /// tester for reset timer not done as purely library function
        /// </summary>
        /// <returns>true if correct otherwise false</returns>
        private bool TestResetTimer()
        {
            return true;
        }

        /// <summary>
        /// stimulates destroy timer object
        /// </summary>
        /// <returns>not implemented because of library function</returns>
        private bool TestDestroyTimerObj()
        {
            return true;
        }

        /// <summary>
        /// stimulates starting of timer object
        /// </summary>
        /// <returns>not implemented because of library function</returns>
        private bool TestCreateTimerObj()
        {
            return true;
        }

        /// <summary>
        /// test method for chcecking if it works for a correct image
        /// </summary>
        /// <returns>true upon getting correction</returns>
        private bool CheckOnImageDecoded()
        {
            this.ReceiveImage.CreateTimerObj();
            this.ReceiveImage.OnRecievingimage("true", this.IP);
            this.ReceiveImage.DestroyTimerObj();
            if (this.UIImageFlag)
            {
                this.UIImageFlag = false;
                this.Logger.LogInfo("On image decoded function passed test");
                return true;
            }

            this.Logger.LogInfo("On image decoded function failed test");
            return false;
        }

        /// <summary>
        /// test method for chcecking if it works for a error passed by receiveImage
        /// </summary>
        /// <returns>true upon getting correction</returns>
        private bool CheckOnErrorDecoded()
        {
            this.ReceiveImage.OnRecievingimage("false", this.IP);
            if (this.UIErrorFlag)
            {
                this.Logger.LogInfo("On error decoded function passed test");
            }
            else
            {
                this.Logger.LogInfo("On error decoded function failed test");
            }

            return this.UIErrorFlag;
        }

        /// <summary>
        /// decodes the image using schema and compression stubs
        /// </summary>
        /// <returns>returns true upon getting expected output</returns>
        private bool TestGetBitmap()
        {
            if (this.FailedToDecodeBySchema() && this.FailedToConvertStringToBitmap() && this.FailedToDecompress())
            {
                this.Logger.LogInfo("getBitmap passed the test");
                return true;
            }
            else
            {
                this.Logger.LogInfo("getBitmap failed the test");
                return false;
            }
        }

        /// <summary>
        /// this method checks upon getting message from upper module how it handles
        /// </summary>
        /// <returns>true upon parsing correctly</returns>
        private bool TestOnReceiveImage()
        {
            if (this.CheckOnErrorDecoded() && this.CheckOnImageDecoded())
            {
                this.Logger.LogInfo("On receive image passed the test");
                return true;
            }

            this.Logger.LogInfo("On receive image failed the test");
            return false;                
        }

        /// <summary>
        /// this method checks whether program gets error while changing from raw string to some dicitonary by schema stub
        /// </summary>
        /// <returns>return false upon failing</returns>
        private bool FailedToConvertStringToBitmap()
        {
            this.ReceiveImage.OnRecievingimage("ex_bmpstring", this.IP);
            return this.ReceiveImage.Error.ErrorMessage == "Image decompression returned exception";
        }

        /// <summary>
        /// this method checks whether program gets error while changing from string to image map by compression class
        /// </summary>
        /// <returns>return true upon encountering error</returns>
        private bool FailedToDecompress()
        {
            this.ReceiveImage.OnRecievingimage("ex_compress", this.IP);
            return this.ReceiveImage.Error.ErrorMessage == "Image decompression returned exception";
        }

        /// <summary>
        /// this method checks whether program gets error while changing from dictionary to image map by compression class
        /// </summary>
        /// <returns>return true upon encountering error</returns>
        private bool FailedToDecodeBySchema()
        {
            this.ReceiveImage.OnRecievingimage("ex_schema", this.IP);
            return this.ReceiveImage.Error.ErrorMessage == "Schema unable to decode";
        }

        /// <summary>
        /// method checks whether stop signal and resend signal works fine
        /// </summary>
        /// <returns>true if both works</returns>
        private bool FailedImagePassing()
        {
            if (this.CheckResendSignal() && this.CheckStopSignal())
            {
                this.Logger.LogInfo("failed image passing function passed the test");
                return true;
            }

            this.Logger.LogInfo("failed image passing function failed the test");
            return false;
        }

        /// <summary>
        /// method checks whether the timer trigger stop signal after intended time interval
        /// </summary>
        /// <returns>return true upon stop signal by both driver and stub image communication</returns>
        private bool CheckStopSignal()
        {
            this.ReceiveImage.CreateTimerObj();
            this.manualReset.WaitOne();
            this.ReceiveImage.DestroyTimerObj();
            if (this.StopSignal && this.ImageCommunication.Signal == Signal.Stop)
            {
                this.Logger.LogInfo("stop signal passed the test");
                return true;
            }

            this.Logger.LogInfo("stop signal failed the test");
            return false;
        }

        /// <summary>
        /// upon gettin error image checks whether it sends resend signal to stub image communication
        /// </summary>
        /// <returns>return true upon resend signal</returns>
        private bool CheckResendSignal()
        {
            this.ReceiveImage.OnRecievingimage("false", this.IP);
            if (this.ImageCommunication.Signal == Signal.Resend)
            {
                this.Logger.LogInfo("resend signal passed the test");
                return true;
            }

            this.Logger.LogInfo("resend signal failed the test");
            return false;
        }

        /// <summary>
        /// upon a successful image driver should report true image flag 
        /// </summary>
        /// <returns>true upon correct execution</returns>
        private bool SuccessfulImagePassing()
        {
            this.ReceiveImage.OnRecievingimage("true", this.IP);
            if (this.UIImageFlag)
            {
                this.UIImageFlag = false;
                this.Logger.LogInfo("SuccessfulImagePassing passed the test");
                return true;
            }

            this.Logger.LogInfo("SuccessfulImagePassing failed the test");
            return false;
        }

        /// <summary>
        /// tests the end to end functionality of receive image class
        /// </summary>
        /// <returns>returns true upon all fucntionality passing</returns>
        private bool ReceiveImageFunctionalityTester()
        {
            if (this.SuccessfulImagePassing() && this.FailedImagePassing())
            {
                this.Logger.LogSuccess("ReceiveImageFunctionalityTester passed the test");
                return true;
            }
            else
            {
                this.Logger.LogError("ReceiveImageFunctionalityTester failed the test");
                return false;
            }
        }
    }
}
