﻿//-----------------------------------------------------------------------
// <author> 
//    Anish M M (anishmathewdev@gmail.com)
// </author>
//
// <date> 
//     16th November, 2018
// </date>
// 
// <reviewer> 
//
// </reviewer>
// 
// <copyright file="RegisterAndRetrieveTelemetryTest.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//    This class tests whether registering and retrieval of telemetry with
//    the TelemetryCollector works correctly.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.QualityAssurance.TelemetryTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class to test retreival and register functionalities of telemetry.
    /// </summary>
    public class RegisterAndRetrieveTelemetryTest : ITest
    {
        /// <summary>
        /// Logger instance used to log messages while testing.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegisterAndRetrieveTelemetryTest" /> class.
        /// </summary>
        /// <param name="logger">The logger instance to be used to log messages while testing.</param>
        public RegisterAndRetrieveTelemetryTest(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Runs tests to check register and retrieval functionalities of telemetry.
        /// </summary>
        /// <returns>Combined success status of the tests.</returns>
        public bool Run()
        {
            ITelemetryCollector telemetryCollector = TelemetryCollector.Instance;
            SampleTelemetry sampleTelemetry = new SampleTelemetry();

            // Register new telemetry object. Should succeed.
            this.logger.LogInfo("Registering new telemetry instance...");

            bool registeredSuccessfully = telemetryCollector.RegisterTelemetry("SampleTelemetry", sampleTelemetry);
            
            if (registeredSuccessfully)
            {
                this.logger.LogSuccess("Registered new telemetry successfully. This functionality is working.");
            }
            else
            {
                this.logger.LogWarning("Registering new telemetry failed. Functionality not working as intended!");
            }

            // Rewrite existing object. Should fail.
            this.logger.LogInfo("Overwriting existing telemetry object...");

            bool doesNotOverwriteExisting = !telemetryCollector.RegisterTelemetry("SampleTelemetry", sampleTelemetry);

            if (doesNotOverwriteExisting)
            {
                this.logger.LogSuccess("Overwriting failed. Functionality working as intended.");
            }
            else
            {
                this.logger.LogWarning("Overwrite succeeded. Functionality not working as intended!");
            }

            // Register another object. Should succeed.
            this.logger.LogInfo("Registering another telemetry...");

            sampleTelemetry.IncrementQuantity();

            bool registeredMultipleSuccessfully = telemetryCollector.RegisterTelemetry("NewSampleTelemetry", sampleTelemetry);

            if (registeredMultipleSuccessfully)
            {
                this.logger.LogSuccess("Registered another telemetry. Functionality working as expected.");
            }
            else
            {
                this.logger.LogWarning("Did not register another telemetry. Functionality not working as intended!");
            }

            // Retrieve objects stored earlier. Should hold same values.
            this.logger.LogInfo("Retrieving telemetry stored earlier...");

            var retrievedStoredTelemetryCorrectly = false;
            var retrievedNewSampleTelemetry = (SampleTelemetry)telemetryCollector.GetTelemetryObject("NewSampleTelemetry");
            var retrievedSampleTelemetry = (SampleTelemetry)telemetryCollector.GetTelemetryObject("SampleTelemetry");

            if (retrievedNewSampleTelemetry == null || retrievedSampleTelemetry == null)
            {
                this.logger.LogWarning("Did not retrieve stored telemetry. Functionality not working as intended.");
            }
            else
            {
                var currentQuantity = int.Parse(sampleTelemetry.DataCapture["Quantity"], CultureInfo.CurrentCulture);
                var newQuantityValue = int.Parse(retrievedNewSampleTelemetry.DataCapture["Quantity"], CultureInfo.CurrentCulture);
                var quantityValue = int.Parse(retrievedSampleTelemetry.DataCapture["Quantity"], CultureInfo.CurrentCulture);

                // Check if values are correct.
                if (quantityValue == currentQuantity && newQuantityValue == currentQuantity)
                {
                    this.logger.LogSuccess("Retrieved correctly. Functionality working as intended.");
                    retrievedStoredTelemetryCorrectly = true;
                }
                else
                {
                    this.logger.LogWarning("Retrieved wrong values. Functionality not working as intended!");
                }
            }

            // Request non-existing telemetry. Should return null.
            this.logger.LogInfo("Retrieving non-existing telemetry...");

            var nullOnNonExisting = false;
            var nonExistingTelemetry = (SampleTelemetry)telemetryCollector.GetTelemetryObject("ThisSampleTelemetryDoesNotExist");

            if (nonExistingTelemetry == null)
            {
                this.logger.LogSuccess("Returned null object. Functionality working as intended.");
                nullOnNonExisting = true;
            }
            else
            {
                this.logger.LogWarning("Returned non-null object for non-existing telemetry. Functionality not working as intended!");
            }

            return registeredSuccessfully &&
                doesNotOverwriteExisting &&
                registeredMultipleSuccessfully &&
                retrievedStoredTelemetryCorrectly &&
                nullOnNonExisting;
        }
    }
}
